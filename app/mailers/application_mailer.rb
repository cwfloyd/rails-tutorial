class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@thirdreich.org'
  layout 'mailer'
end
