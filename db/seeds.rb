# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: "Adolf Hitler", 
             email: "adolf.hitler.89@yandex.ru",
             password: "siegheil",
             password_confirmation: "siegheil",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name: "Joseph Goebbels", 
             email: "joe.goebbels@yandex.ru",
             password: "siegheil",
             password_confirmation: "siegheil",
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name: "Hermann Goering", 
             email: "herm.goering@yandex.ru",
             password: "siegheil",
             password_confirmation: "siegheil",
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name: "Heinrich Himmler", 
             email: "heiny.himmler@yandex.ru",
             password: "siegheil",
             password_confirmation: "siegheil",
             activated: true,
             activated_at: Time.zone.now)

User.create!(name: "Martin Bormann", 
             email: "marty.bormann@yandex.ru",
             password: "siegheil",
             password_confirmation: "siegheil",
             activated: true,
             activated_at: Time.zone.now)
             
96.times do |n|
  name = Faker::Name.name
  email = "white_nationalist.#{n+4}@fourthreich.org"
  password = "siegheil"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::StarWars.quote
  users.each { |user| user.microposts.create!(content: content) }
end

users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }


