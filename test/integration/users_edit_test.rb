require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:adolf)
    @other_user = users(:joseph)
  end
  
  test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert !flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect to home when logged in user attempts to edit another user's page" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect update when not logged in" do
    patch user_path(@user), params: { user: {name: @user.name,
                                             email: @user.email } }
    assert !flash.empty?
    assert_redirected_to login_url
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name: "",
                                              email: "adolf@hitler",
                                              password: "sieg",
                                              password_confirmation: "heil" } }
    assert_template 'users/edit'
    assert_select 'div.alert', text: 'The form contains 4 errors'
  end

  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    assert session[:forwarding_url].nil?
    name = "Adolf Hitler"
    email = "ahitler@thirdreich.org"
    patch user_path(@user), params: { user: { name: name,
                                              email: email,
                                              password: "",
                                              password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
end
