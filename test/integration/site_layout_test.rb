require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  require 'test_helper'
    def setup
      @user = users(:adolf)
    end
  
    test "layout links without login" do
      get root_path
      assert_template 'static_pages/home'
      assert_select "a[href=?]", root_path, count: 2
      assert_select "a[href=?]", help_path
      assert_select "a[href=?]", login_path
      assert_select "a[href=?]", about_path
      assert_select "a[href=?]", contact_path
    end
    
     test "layout links with login" do
      get login_path
      log_in_as(@user)
      get root_path
      assert_template 'static_pages/home'
      assert_select "a[href=?]", root_path, count: 2
      assert_select "a[href=?]", help_path
      assert_select "a[href=?]", users_path
      assert_select "a[href=?]", users_path
      assert_select "a[href=?]", about_path
      assert_select "a[href=?]", contact_path
    end
end
