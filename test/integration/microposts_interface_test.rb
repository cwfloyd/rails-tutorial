require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:adolf)
  end
  
  test "micropost interface" do
    log_in_as(@user)
    get root_path
    assert_select "div.pagination"
    assert_select "input[type=file]"
    assert_match ActionController::Base.helpers.pluralize(@user.microposts.count, 
                                                      "micropost"), response.body
    #Invalid submission
    assert_no_difference "Micropost.count" do
      post microposts_path, params: { micropost: { content: "" } }
    end
    assert_select "div#error_explanation"
    #Valid submission
    content = "Make the lie big, make it simple, keep saying it, and eventually 
               they will believe it."
    picture = fixture_file_upload("test/fixtures/swastika.jpg", "image/jpg")
    assert_difference "Micropost.count", 1 do
      post microposts_path, params: { micropost: {content: content, picture: picture } }
    end
    # assert_redirected_to root_url
    micropost = assigns(:micropost)
    assert micropost.picture?
    follow_redirect!
    assert_match content, response.body
    #Delete post
    assert_select 'a', text: "delete"
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    #Visit different user (no delete links)
    get user_path(users(:joseph))
    assert_select "a", text: "delete", count: 0
  end
end
