require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name:"Any User", email:"any@email.net",
                    password: "secure", password_confirmation: "secure")
  end
  test "user should start valid" do
    assert @user.valid?
  end
  test "name should be present" do
    @user.name = "   "
    assert_not @user.valid?
  end
  test "email should be present" do
    @user.email = "   "
    assert_not @user.valid?
  end
  test "name shouldn't be too long" do
    @user.name = "i" * 51
    assert_not @user.valid?
  end
  test "email shouldn't be too long" do
    @user.email = "i" * 256
    assert_not @user.valid?
  end  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                        first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"  
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                          foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  test "password should be present" do
    @user.password = " " * 6
    assert_not @user.valid?
  end
  test "password should be at least 6 characters long" do
    @user.password = @user.password_confirmation = "i" * 5
    assert_not @user.valid?
  end
  test "authenticated? should return false for a user with a nil digest" do
    assert_not @user.authenticated?(:remember, "")
  end
  
  test "destroying a user should destroy his microposts" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference "Micropost.count", -1 do
      @user.destroy
    end
  end
  
  test "should follow and unfollow a user" do
    adolf = users(:adolf)
    joseph = users(:joseph)
    assert_not adolf.following?(joseph)
    adolf.follow(joseph)
    adolf.reload
    assert adolf.following(joseph)
    adolf.unfollow(joseph)
    adolf.reload
    assert_not adolf.following?(joseph)
  end
  
  test "feed should have the right posts" do
    adolf = users(:adolf)
    hermann = users(:hermann) #followed - see relationships fixture
    joseph = users(:joseph) #not followed
    #Posts from followed user
    hermann.microposts.each do |post_following|
      assert adolf.feed.include?(post_following)
    end
    #Posts from self
    adolf.microposts.each do |post_self|
      assert adolf.feed.include?(post_self)
    end
    #Posts from unfollowed user
    joseph.microposts.each do |post_unfollowed|
      assert_not adolf.feed.include?(post_unfollowed)
    end
  end
end

