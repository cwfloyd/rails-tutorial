require 'test_helper'

class MicropostTest < ActiveSupport::TestCase

  def setup
    @user = users(:adolf)
    @micropost = @user.microposts.build(content: "lorem ipsum")
  end

  test "should be valid" do
    assert @micropost.valid?
  end

  test "user id should be present" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end
  
  test "post should be present" do
    @micropost.content = ""
    assert_not @micropost.valid?
  end
  
  test "post length should not exceed 140 characters" do
    @micropost.content = "c" * 141
    assert_not @micropost.valid?
  end
  
  test "order should be most recent first" do
    assert_equal microposts(:most_recent), Micropost.first
  end
end
