require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:adolf)
    @other_user = users(:joseph)
  end

  test "should redirect index when not logged in" do
    get users_path
    assert_redirected_to login_url
  end

  test "should redirect to home when logged in user attempts to update another user's page" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: { name: @user.name,
                                                  email: @user.email } }
    assert flash.empty?
    assert_redirected_to root_url
  end

  #??? last line
  test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch user_path(@other_user), params: {
                                    user: { password: 'oy-vey',
                                            password_confirmation: 'oy-vey',
                                            admin: true } }
    assert_not @other_user.admin?
  end

  test "non logged in users should be redirected to the sign in page" do
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end
  
  test "non admins should be redirected to the home page" do
    log_in_as(@other_user)
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
  end
  
  test "should get new" do
    get signup_path
    assert_response :success
  end
  
  test "should get index" do
    get login_path
    log_in_as(@user)
    get users_path
    assert_response :success
  end
  
  test "should redirect following when not logged in" do
    get following_user_path(@user)
    assert_redirected_to login_url
  end

  test "should redirect followers when not logged in" do
    get followers_user_path(@user)
    assert_redirected_to login_url
  end
end
